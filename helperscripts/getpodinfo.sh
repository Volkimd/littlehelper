#!/bin/bash

K8SAPPLICATION=$1

PODNAME=$(kubectl get pods -A -l app=$K8SAPPLICATION --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | head -1 2> $K8SAPPLICATION/PODERROR.log)   
DAEMONSETNAME=$(kubectl get daemonset -A -l app=$K8SAPPLICATION --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | head -1 2> $K8SAPPLICATION/DSERROR.log)   
DEPLOYMENTNAME=$(kubectl get deployment -A -l app=$K8SAPPLICATION --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | head -1 2> $K8SAPPLICATION/DEPERROR.log)   
CONFIGMAPNAME=$(kubectl get configmap -A -l app=$K8SAPPLICATION --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | head -1 2> $K8SAPPLICATION/CMERROR.log)   
PODNAMESPACE=$(kubectl get pods -A -l app=$K8SAPPLICATION --template '{{range .items}}{{.metadata.namespace}}{{"\n"}}{{end}}' | head -1  2> $K8SAPPLICATION/PODNSERROR.log)  


mkdir $K8SAPPLICATION


kubectl get daemonset $DAEMONSETNAME -n $PODNAMESPACE -oyaml > $K8SAPPLICATION/$K8SAPPLICATION-POD.yaml
if [[ $DAEMONSETNAME ]];then
    kubectl get daemonset $DAEMONSETNAME -n $PODNAMESPACE -oyaml > $K8SAPPLICATION/$K8SAPPLICATION-DS.yaml
fi
if [[ $DEPLOYMENTNAME ]];then
    kubectl get deployment $DEPLOYMENTNAME -n $PODNAMESPACE -oyaml > $K8SAPPLICATION/$K8SAPPLICATION-DEPLOYMENT.yaml
fi
if [[ $CONFIGMAPNAME ]];then
    kubectl get configmap $CONFIGMAPNAME -n $PODNAMESPACE -oyaml > $K8SAPPLICATION/$K8SAPPLICATION-CONFIGMAP.yaml
fi


if [[ $K8SAPPLICATION -eq "fluentd"]];then
    kubectl exec $PODNAME -n $PODNAMESPACE -c rancher-logging-fluentd -- fluentd --version -n > $K8SAPPLICATION/fluentdversion.txt
    kubectl exec $PODNAME -n $PODNAMESPACE -c rancher-logging-fluentd -- cat /etc/fluent/fluent.conf > $K8SAPPLICATION/fluentdconfig.txt
fi



# tar -zcvf $K8SAPPLICATION.tar.gz $K8SAPPLICATION & sleep 20 & rm -r $K8SAPPLICATION


